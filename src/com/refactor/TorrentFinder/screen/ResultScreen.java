package com.refactor.TorrentFinder.screen;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.refactor.TorrentFinder.R;
import com.refactor.TorrentFinder.model.Fenopy;
import com.refactor.TorrentFinder.model.IsoHunt;
import com.refactor.TorrentFinder.model.Site;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ResultScreen extends Activity {
    private String query;
    private List<Fenopy> fenopyList;
    private List<IsoHunt> isoHuntList;
    private String url;
    private Site site;
    private ProgressDialog dialog;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result);

        getExtras();
        startSiteTaskFromChoice();
    }

    private void getExtras() {
        Bundle extras = getIntent().getExtras();
        query = extras.getString("query");
        site = (Site) extras.get("site");
    }

    private void startSiteTaskFromChoice() {
        dialog = new ProgressDialog(ResultScreen.this);
        switch (site) {
            case FENOPY:
                fenopyList = new ArrayList<Fenopy>();
                url = "http://fenopy.se/module/search/api.php?" + "keyword=" + query + "&format=json";
                dialog.setMessage("Please wait...");
                dialog.show();
                new FenopyTask().execute();
                break;
            case ISOHUNT:
                isoHuntList = new ArrayList<IsoHunt>();
                url = "http://isohunt.com/js/json.php?ihq=" + query + "&sort=seeds&row=2";
                dialog.setMessage("Please wait...");
                dialog.show();
                new IsoHuntTask().execute();
                break;
        }
    }

    private String getJSONData() {
        HttpClient client = new DefaultHttpClient();
        HttpGet get = new HttpGet(url);
        HttpResponse response;
        StringBuilder json = new StringBuilder();

        try {
            response = client.execute(get);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream is = entity.getContent();
                String line;
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                while ((line = reader.readLine()) != null) {
                    json.append(line + "\n");
                }
                reader.close();
                is.close();
            }
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return json.toString();
    }

    private class FenopyTask extends AsyncTask<Void, Void, Void> {

        private void intoListJSONData() {
            String json = getJSONData();
            try {
                JSONArray array = new JSONArray(json);
                for (int i=0; i<array.length(); i++) {
                    JSONObject object = array.getJSONObject(i);
                    Fenopy fenopy = new Fenopy();
                    /*
                        if (object.getInt("verified") == 1) {
                            fenopy.setVerified("Yes");
                        } else {
                            fenopy.setCategory("No");
                        }
                    */
                    fenopy.setCategory(object.getString("category"));
                    fenopy.setLeecher(object.getInt("leecher"));
                    fenopy.setName(object.getString("name"));
                    fenopy.setSeeder(object.getInt("seeder"));
                    fenopy.setSize(object.getInt("size"));
                    fenopy.setTorrent(object.getString("torrent"));
                    fenopyList.add(fenopy);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            intoListJSONData();
            return null;
        }

        @Override
        protected void onPostExecute(Void results) {
            ListView listView = (ListView)findViewById(R.id.resultListView);
            FenopyAdapter adapter = new FenopyAdapter(ResultScreen.this, R.layout.fenopylist, fenopyList);
            listView.setAdapter(adapter);
            dialog.dismiss();
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent intent = new Intent(Intent.ACTION_DEFAULT, Uri.parse(fenopyList.get(i).getTorrent()));
                    startActivity(intent);
                }
            });
        }
    }

    private class FenopyAdapter extends ArrayAdapter {
        public FenopyAdapter(Context context, int textViewResourceId, List<Fenopy> list) {
            super(context, textViewResourceId, list);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View row = convertView;

            if(row == null){
                LayoutInflater inflater = getLayoutInflater();
                row = inflater.inflate(R.layout.fenopylist, null);
            }

            TextView name = (TextView)row.findViewById(R.id.name);
            name.setText(fenopyList.get(position).getName());

            /*
                TextView verified = (TextView)row.findViewById(R.id.verified);
                verified.setText("Verified: " + fenopyList.get(position).getVerified());
            */

            TextView size = (TextView)row.findViewById(R.id.size);
            size.setText("Size: " + String.valueOf(fenopyList.get(position).getSize()) + "MB");

            TextView category = (TextView)row.findViewById(R.id.category);
            category.setText("Category: " + fenopyList.get(position).getCategory());

            TextView seeder = (TextView)row.findViewById(R.id.seeder);
            seeder.setText("Seeder: " + String.valueOf(fenopyList.get(position).getSeeder()));

            TextView leecher = (TextView)row.findViewById(R.id.leecher);
            leecher.setText("Leecher: " + String.valueOf(fenopyList.get(position).getLeecher()));

            return row;
        }
    }

    private class IsoHuntTask extends AsyncTask<Void, Void, Void> {

        private void intoListJSONData() {
            String json = getJSONData();
            try {
                JSONObject object = new JSONObject(json);
                JSONObject items = object.getJSONObject("items");
                JSONArray list = items.getJSONArray("list");

                for(int i=0; i<list.length(); i++) {
                    JSONObject item = list.getJSONObject(i);
                    IsoHunt isoHunt = new IsoHunt();
                    isoHunt.setUrl(item.getString("enclosure_url"));
                    isoHunt.setTitle(item.getString("title").replace("<b>", "").replace("</b>", ""));
                    isoHunt.setSize(item.getString("size"));
                    isoHunt.setSeeds(item.getString("Seeds"));
                    isoHunt.setLeechers(item.getString("leechers"));
                    isoHunt.setDownloads(item.getString("downloads"));
                    isoHunt.setVotes(item.getString("votes"));
                    isoHuntList.add(isoHunt);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            intoListJSONData();
            return null;
        }

        @Override
        protected void onPostExecute(Void results) {
            ListView listView = (ListView)findViewById(R.id.resultListView);
            IsoHuntAdapter adapter = new IsoHuntAdapter(ResultScreen.this, R.layout.isohuntlist, isoHuntList);
            listView.setAdapter(adapter);
            dialog.dismiss();
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent intent = new Intent(Intent.ACTION_DEFAULT, Uri.parse(isoHuntList.get(i).getUrl()));
                    startActivity(intent);
                }
            });
        }
    }

    private class IsoHuntAdapter extends ArrayAdapter {
        public IsoHuntAdapter(Context context, int textViewResourceId, List<IsoHunt> list) {
            super(context, textViewResourceId, list);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View row = convertView;

            if(row == null){
                LayoutInflater inflater = getLayoutInflater();
                row = inflater.inflate(R.layout.isohuntlist, null);
            }

            TextView title = (TextView)row.findViewById(R.id.title);
            title.setText(isoHuntList.get(position).getTitle());

            TextView size = (TextView)row.findViewById(R.id.size);
            size.setText("Size: " + isoHuntList.get(position).getSize() + "MB");

            TextView seeds = (TextView)row.findViewById(R.id.seeds);
            seeds.setText("Seeds: " + String.valueOf(isoHuntList.get(position).getSeeds()));

            TextView leechers = (TextView)row.findViewById(R.id.leechers);
            leechers.setText("Leechers: " + isoHuntList.get(position).getLeechers());

            TextView downloads = (TextView)row.findViewById(R.id.downloads);
            downloads.setText("Downloads: " + String.valueOf(isoHuntList.get(position).getDownloads()));

            TextView votes = (TextView)row.findViewById(R.id.votes);
            votes.setText("Votes: " + String.valueOf(isoHuntList.get(position).getVotes()));

            return row;
        }
    }
}