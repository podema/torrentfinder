package com.refactor.TorrentFinder.screen;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.refactor.TorrentFinder.R;
import com.refactor.TorrentFinder.model.Site;

import java.util.ArrayList;

public class MainScreen extends Activity implements View.OnClickListener {
    private AlertDialog.Builder builder;
    private ArrayList<String> sites;
    private Spinner spinner;
    private Button searchButton;
    private Site site = Site.FENOPY;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        sites = new ArrayList<String>();
        prepareGUIComponents();
    }

    private void prepareGUIComponents() {
        spinner = (Spinner)findViewById(R.id.spinner);
        searchButton = (Button)findViewById(R.id.searchButton);
        searchButton.setOnClickListener(this);

        sites.add("Fenopy");
        sites.add("isoHunt");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sites);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i == 0) {
                    Toast.makeText(getApplicationContext(), "Just search in the Fenopy database", Toast.LENGTH_LONG).show();
                    site = Site.FENOPY;
                } else {
                    Toast.makeText(getApplicationContext(), "Just search in the isoHunt database", Toast.LENGTH_LONG).show();
                    site = Site.ISOHUNT;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void search() {
        Intent intent = new Intent(this, ResultScreen.class);
        String query = ((EditText)findViewById(R.id.editTextQuery)).getText().toString().replaceAll(" ", "+");
        if (query != null && query.length() != 0) {
            intent.putExtra("query", query);
            intent.putExtra("site", site);
            startActivityForResult(intent, 99);
        } else {
            Toast.makeText(getApplicationContext(), "Keyword field is empty", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case R.id.help:
                String message1 = "With this program you can easily find and download torrent files.\n\nTorrent files saved in download folder.";
                builder = new AlertDialog.Builder(this);
                builder.setTitle("What makes this program ?").setIcon(android.R.drawable.ic_dialog_info).setMessage(message1).setPositiveButton("Close", null).show();
                return true;
            case R.id.about:
                String message2 = "Mert Kavi\nmertkavi@gmail.com\nwww.mertkavi.com";
                builder = new AlertDialog.Builder(this);
                builder.setTitle("About").setIcon(android.R.drawable.ic_dialog_info).setMessage(message2).setPositiveButton("Close", null).show();
                return true;
            case R.id.exit:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.searchButton:
                search();
                break;
        }
    }
}
