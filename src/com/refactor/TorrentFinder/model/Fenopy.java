package com.refactor.TorrentFinder.model;

public class Fenopy {
   	private String category;
   	private Number leecher;
   	private String name;
   	private Number seeder;
   	private Number size;
   	private String torrent;
//  private String verified;

 	public String getCategory(){
		return this.category;
	}
	public void setCategory(String category){
		this.category = category;
	}
 	public Number getLeecher(){
		return this.leecher;
	}
	public void setLeecher(Number leecher){
		this.leecher = leecher;
	}
 	public String getName(){
		return this.name;
	}
	public void setName(String name){
		this.name = name;
	}
 	public Number getSeeder(){
		return this.seeder;
	}
	public void setSeeder(Number seeder){
		this.seeder = seeder;
	}
 	public Number getSize(){
		return this.size;
	}
	public void setSize(Number size){
		this.size = Integer.valueOf(String.valueOf(size)) / 1048576;
	}
 	public String getTorrent(){
		return this.torrent;
	}
	public void setTorrent(String torrent){
		this.torrent = torrent;
	}
 	/*
        public String getVerified(){
		    return this.verified;
	    }
	    public void setVerified(String verified){
		    this.verified = verified;
	    }
	*/
}