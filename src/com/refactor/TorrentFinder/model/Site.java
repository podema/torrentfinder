package com.refactor.TorrentFinder.model;

public enum Site {
    FENOPY(1), ISOHUNT(2);
        private int value;

        private Site(int value) {
            this.value = value;
        }
};
